import csv

from os.path import splitext

class BaseCar():

    def __init__(self, car_type,  brand, photo_file_name, carrying) -> None:
        self.car_type = car_type
        self.brand = brand
        self.photo_file_name = photo_file_name
        self.carrying = float(carrying)
        super().__init__()

    def get_photo_file_ext(self):
        return splitext(self.photo_file_name)

class Car(BaseCar):

    def __init__(self, passenger_seats_count, brand, photo_file_name, carrying) -> None:
        self.passenger_seats_count = int(passenger_seats_count)
        super().__init__('car' ,brand, photo_file_name, carrying)

class Truck(BaseCar):

    def __init__(self, body_whl, brand, photo_file_name, carrying) -> None:
        body_whl_list = body_whl.split('x')
        try:
            self.body_width = float(body_whl_list[0])
            self.body_height = float(body_whl_list[1])
            self.body_length = float(body_whl_list[2])
        except ValueError:
            self.body_width = 0.0
            self.body_height = 0.0
            self.body_length = 0.0

        super().__init__('truck', brand, photo_file_name, carrying)

    def get_body_volume(self):
        return self.body_width * self.body_height * self.body_length

class SpecMachine(BaseCar):

    def __init__(self, extra, brand, photo_file_name, carrying) -> None:
        self.extra = extra
        super().__init__('spec_machine', brand, photo_file_name, carrying)


def get_car_list(csv_file):
    car_list = []

    with open(csv_file) as file:
        car_content = csv.reader(file, delimiter=';')
        next(car_content)

        for row in car_content:
            if row:
                type_ = row[0]

                if not type_ == '':
                    brand_ = row[1]
                    photo_file_name_ = row[3]
                    carrying_ = row[5]

                    if type_ == 'car':
                        count_ = row[2]

                        car = Car(count_, brand_, photo_file_name_, carrying_)
                        car_list.append(car)
                    elif type_ == 'truck':
                        whl_ = row[4]

                        truck = Truck(whl_, brand_, photo_file_name_, carrying_)
                        car_list.append(truck)
                    elif type_ == 'spec_machine':
                        extra_ = row[5]

                        spec_machine = SpecMachine(extra_, brand_, photo_file_name_, carrying_)
                        car_list.append(spec_machine)

        return car_list


if __name__ == '__main__':
    car_list = get_car_list('coursera_week3_cars.csv')

    truck_01 = car_list[1]
    truck_02 = car_list[2]

    print(truck_01.get_body_volume())
    print(truck_02.get_body_volume())
