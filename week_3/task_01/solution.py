class FileReader():

    def __init__(self, path_file):
        self.path_file = path_file

    def read(self):
        try:
            with open(self.path_file, "r") as file:
                read = file.read()
                return str(read)
        except IOError:
            return ""
