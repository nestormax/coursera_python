import tempfile
import os

class File():
    def __init__(self, file_path) -> None:
        self.file_path = file_path
        self.current_line_num = 0
        super().__init__()

    def __add__(self, obj):
        path = tempfile.gettempdir()
        file_path = os.path.join(path, '123.txt')

        new_file = File(file_path)
        content = self.read() + (obj.read())
        new_file.write(content)

        return new_file

    def __eq__(self, obj) -> bool:
        return self.file_path == obj.file_path

    def __hash__(self) -> int:
        return hash(self.file_path)

    def __str__(self) -> str:
        return self.file_path

    def __iter__(self):
        return self

    def __next__(self):
        list_line = self.read().split('\n')
        self.end_line = len(list_line)

        if self.current_line_num >= self.end_line:
            raise StopIteration

        current_line = list_line[self.current_line_num]
        self.current_line_num += 1

        return current_line

    def read(self):
        with open(self.file_path, 'r') as file:
            return file.read()

    def write(self, content):
        with open(self.file_path, 'a') as file:
            file.write(content)

if __name__ == '__main__':
    file_1 = File('test.txt')
    file_2 = File('test_2.txt')

    file_3 = file_1 + file_2
    # file_1.write("Max\n")
    print(file_3)
    for line in file_3:
        print(line)

    print(len(file_3.read().split('\n')))