class Value():

    def __get__(self, instance, owner):
        return self.amount

    def __set__(self, instance, value):
        self.amount = value - int(value * instance.commission)

class Account:
    amount = Value()

    def __init__(self, commission) -> None:
        self.commission = commission