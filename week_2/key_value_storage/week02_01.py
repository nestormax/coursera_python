import argparse
import tempfile
import json
import os

parser = argparse.ArgumentParser()
storage_path = os.path.join(tempfile.gettempdir(), 'storage.data')

if not os.path.isfile(storage_path):
    with open(storage_path, "w") as file:
        file.write(json.dumps({}))

parser.add_argument('--key', type=str)
parser.add_argument("--val")
args = parser.parse_args()

key = args.key
val = args.val

def get(key):
    print(get_value(key))

def value_json(key, val):
    return {
        key: val
    }


def add_value(key, val):
    with open(storage_path, "r") as file:
        read = file.read()
        json_storage = json.loads(read)

        if key in json_storage:
            value_list = json_storage[key]
            value_list.append(val)

        else:
            value_list = [val]
            json_storage[key] = value_list


    with open(storage_path, 'w') as file:
        dumps = json.dumps(json_storage)
        file.write(dumps)


def get_value(key):
    with open(storage_path, "r") as file:
        read = file.read()
        data = json.loads(read)

        if key in data:
            values = data[key]
            return ', '.join(str(val) for val in values)
        else:
            return None

if val is not None:
    add_value(key, val)
else:
    get(key)

